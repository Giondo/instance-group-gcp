resource "google_compute_firewall" "firewall-docker" {
  name    = "firewall-docker"
  network = google_compute_network.test.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22"]
  }

  target_tags = ["docker"]
  source_ranges = ["0.0.0.0/0"]
}
