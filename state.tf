terraform {
  backend "gcs" {
    bucket  = "equifax-demo-tfstate"
    prefix  = "terraform/state/instance-with-docker"
  }
}
