output "vmname" {
	value = google_compute_instance_template.dockervm.name
}
 output "ExternalIP" {
 	value = google_compute_address.static_ext_ip.address
 }
 output "GlobalLoadBalancer" {
 	value = google_compute_global_forwarding_rule.default.ip_address
 }
#output "Internal-LoadBalancer" {
#	value = google_compute_forwarding_rule.internal-fw.ip_address
#}
