resource "google_compute_instance_template" "dockervm" {
    name_prefix  = "dockervm"
    machine_type = "n1-standard-2"
    tags = ["docker"]
    description = "Instance with docker engine"

    scheduling {
       automatic_restart   = true
       on_host_maintenance = "MIGRATE"
     }

     disk {
         source_image = "debian-cloud/debian-9"
         auto_delete  = true
         boot         = true
     }

    network_interface {
      network = google_compute_network.test.name
      access_config {
            nat_ip = google_compute_address.static_ext_ip.address
        }
      }


      labels = {
          environment = "dev"
          owner = "david"
          project = "docker"
        }
    metadata_startup_script = file("scripts/init_script-docker.sh")
    service_account {
      email  = "registry-read@equifax-demo-258920.iam.gserviceaccount.com"
      scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }



}
