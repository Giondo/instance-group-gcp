resource "google_compute_region_instance_group_manager" "instance_group_manager" {
  name               = "dockergroup"
  base_instance_name = "dockervm"
  region             = var.REGION
  target_size        = "1"
  distribution_policy_zones  = var.ZONES
  version {
    name = "primary"
    instance_template  = google_compute_instance_template.dockervm.self_link
  }

  auto_healing_policies {
    health_check      = google_compute_health_check.autohealing.self_link
    initial_delay_sec = 300
  }

}
