resource "google_compute_region_autoscaler" "autoscaler" {
  name   = "my-autoscaler"
  region = var.REGION
  target = google_compute_region_instance_group_manager.instance_group_manager.self_link

  autoscaling_policy {
    max_replicas    = 2
    min_replicas    = 1
    cooldown_period = 60

    cpu_utilization {
      target = 0.5
    }
  }
}
