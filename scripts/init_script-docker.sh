#!/bin/bash
#update apt
apt-get update -y
#Install a few prerequisite packages which let apt use packages over HTTPS:
apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
#Add the GPG key for the official Docker repository to your system:
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
#Add the Docker repository to APT sources:
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
#update the package database with the Docker packages from the newly added repo:
apt update -y
#Make sure you are about to install from the Docker repo instead of the default Debian repo:
apt-cache policy docker-ce -y
# install Docker:
apt install docker-ce -y
#run a docker application
docker run -d -p 80:80 prakhar1989/static-site
