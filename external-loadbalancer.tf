resource "google_compute_global_forwarding_rule" "default" {
  name       = "global-rule"
  target     = google_compute_target_http_proxy.default.self_link
  port_range = "80"
}

resource "google_compute_target_http_proxy" "default" {
  name        = "target-proxy"
  description = "a description"
  url_map     = google_compute_url_map.default.self_link
}

resource "google_compute_url_map" "default" {
  name            = "url-map-target-proxy"
  description     = "a description"
  default_service = google_compute_backend_service.backend1.self_link

  host_rule {
    hosts        = ["mysite.com"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = google_compute_backend_service.backend1.self_link

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_service.backend1.self_link
    }
  }
}

resource "google_compute_backend_service" "backend1" {
  name        = "backend1"
  port_name   = "http"
  protocol    = "HTTP"
  timeout_sec = 10
  # target      = google_compute_region_instance_group_manager.instance_group_manager.self_link
  backend {
    group = google_compute_region_instance_group_manager.instance_group_manager.instance_group
  }

  health_checks = [google_compute_health_check.autohealing.self_link]
}
